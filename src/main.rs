use bme280::i2c::BME280;
use bme280::Configuration;
use clap::Parser;
use rppal::{hal::Delay, i2c::I2c};
use rumqttc::v5::{mqttbytes::QoS, AsyncClient, MqttOptions};
use std::time::Duration;
use tokio::time::sleep;
use tracing::{debug, error, info};

mod args;
mod bme;
mod ha;

use ha::HaInit;

use anyhow::Result;

async fn publish_metrics(
    bme: &mut BME280<I2c>,
    delay: &mut Delay,
    client: &AsyncClient,
    wait_time: Duration,
    topics_prefix: &str,
) -> anyhow::Result<()> {
    let temp_topic = format!("{}/temp", topics_prefix);
    let preassuer_topic = format!("{}/preassure", topics_prefix);

    loop {
        let measurment = bme.measure(delay).unwrap();
        info!("Read temp: {measurment:?} , sending to {temp_topic:?}");
        client
            .publish(
                temp_topic.clone(),
                QoS::ExactlyOnce,
                false,
                measurment.temperature.to_string(),
            )
            .await?;
        client
            .publish(
                preassuer_topic.clone(),
                QoS::ExactlyOnce,
                false,
                measurment.pressure.to_string(),
            )
            .await?;
        sleep(wait_time).await;
    }
}

#[tokio::main]
async fn main() -> Result<()> {
    tracing_subscriber::fmt::init();
    // read command line args
    let args = args::Args::parse();

    debug!("inputs: {args:#?}");

    // set up mqtt client
    let mut mqttopts = MqttOptions::new(args.name.clone(), args.addr, args.port);
    mqttopts.set_credentials(args.username, args.password);
    let (client, mut eventloop) = AsyncClient::new(mqttopts, 100);

    let i2c_bus = I2c::new()?;
    // initialize the BME280 using the primary I2C address
    let mut bme = BME280::new_primary(i2c_bus);
    let bme_config = Configuration::default()
        .with_temperature_oversampling(bme280::Oversampling::Oversampling16X)
        .with_pressure_oversampling(bme280::Oversampling::Oversampling16X);

    let mut delay = Delay;
    bme.init_with_config(&mut delay, bme_config).unwrap();
    let wait_time = Duration::from_secs(args.delay);

    // Initialize ha autodiscovery
    let ha_init_temp = HaInit::new(
        args.name,
        format!("{}/temp", args.topic.clone()),
        "°C".into(),
        "temperature".into(),
    );
    client
        .publish(
            format!("homeassistant/sensor/{}/config", ha_init_temp.object_id()),
            QoS::ExactlyOnce,
            true,
            serde_json::to_string(&ha_init_temp)?,
        )
        .await?;

    // background loop the enventloop
    tokio::task::spawn(async move {
        loop {
            match eventloop.poll().await {
                Ok(notification) => debug!("Received = {:?}", notification),
                Err(err) => error!("Error: {}", err),
            };
        }
    });

    // blockingly call publish
    loop {
        match publish_metrics(&mut bme, &mut delay, &client, wait_time, &args.topic).await {
            Ok(_) => error!("somehow left publishing loop"),
            Err(e) => error!("Error generated while publishing metrics: {}", e),
        }
    }
}
